import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborativeSpacesComponent } from './collaborative-spaces.component';

describe('CollaborativeSpacesComponent', () => {
  let component: CollaborativeSpacesComponent;
  let fixture: ComponentFixture<CollaborativeSpacesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaborativeSpacesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CollaborativeSpacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
