import { Component, OnInit } from '@angular/core';
import { CollaborativeSpacesService } from './collaborative-spaces.service';

@Component({
  selector: 'app-collaborative-spaces',
  templateUrl: './collaborative-spaces.component.html',
  styleUrls: ['./collaborative-spaces.component.css']
})
export class CollaborativeSpacesComponent implements OnInit {
  collaborativeSpaces: any[] = [];

  constructor(private collaborativeSpacesService: CollaborativeSpacesService) {}

  ngOnInit(): void {
    this.fetchCollaborativeSpaces();
  }

  fetchCollaborativeSpaces(): void {
    this.collaborativeSpacesService.getCollaborativeSpaces().subscribe(spaces => {
      this.collaborativeSpaces = spaces;
    });
  }
}
