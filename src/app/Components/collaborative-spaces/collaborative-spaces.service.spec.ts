import { TestBed } from '@angular/core/testing';

import { CollaborativeSpacesService } from './collaborative-spaces.service';

describe('CollaborativeSpacesService', () => {
  let service: CollaborativeSpacesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CollaborativeSpacesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
