import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CollaborativeSpacesService {
  private apiUrl = 'your_backend_api/collaborative-spaces';

  constructor(private http: HttpClient) {}

  getCollaborativeSpaces(): Observable<any[]> {
    // Implement logic to fetch collaborative spaces from the backend
    return this.http.get<any[]>(`${this.apiUrl}/get-spaces`);
  }

  createCollaborativeSpace(spaceData: any): Observable<any> {
    // Implement logic to create a new collaborative space and communicate with the backend
    return this.http.post(`${this.apiUrl}/create-space`, spaceData);
  }
}
