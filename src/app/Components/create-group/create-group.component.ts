import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubredditModel } from '../../Services/subreddit.response';
import { SubredditService } from '../../Services/subreddit.service';
import { Router } from '@angular/router';
import { UserProfileService } from '../user-profile/user-profile.service';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {
  
  createSubredditForm!: FormGroup;
  subredditModel!: SubredditModel;
  employeeId: number | undefined;
  isSubmitting: boolean = false; // Flag to track submission status

  constructor(private router: Router, 
              private subredditService: SubredditService,
              private userProfileService: UserProfileService,
              private formBuilder: FormBuilder) {
    this.createSubredditForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['']
    });
    this.subredditModel = {
      groupName: '',
      description: '',
      employeeId: 0, // Default value
      userName: '' // Placeholder value, will be updated later
    };
  }

  ngOnInit(): void {
    // Fetch employeeId when the component initializes
    this.fetchEmployeeId();
  }

  fetchEmployeeId(): void {
    let userId = localStorage.getItem('userId');
    if (userId !== null) {
      this.userProfileService.getUserProfile(userId).subscribe(
        (data: any) => {
          this.employeeId = data.id;
          this.subredditModel.userName = data.employeeId;
        },
        (error: any) => {
          console.error('Error fetching user profile:', error);
        }
      );
    }
  }

  createSubreddit() {
    if (this.employeeId && !this.isSubmitting) { // Check if not already submitting
      const titleControl = this.createSubredditForm.get('title');
      const descriptionControl = this.createSubredditForm.get('description');
      if (titleControl && descriptionControl) {
        this.subredditModel.groupName = titleControl.value;
        this.subredditModel.description = descriptionControl.value;
        this.subredditModel.employeeId = this.employeeId;
        this.isSubmitting = true; // Set flag to true when starting submission

        this.subredditService.createSubreddit(this.subredditModel).subscribe(
          (data: any) => {
            console.log('Group created:', data);
            this.isSubmitting = false; // Reset flag after successful submission
            this.router.navigateByUrl('/list-group');
          },
          (error: any) => {
            console.error('Error occurred while creating subreddit:', error);
            this.isSubmitting = false; // Reset flag after error
          }
        );
      } else {
        console.error('Form controls are null');
      }
    } else {
      console.error('Employee ID not available or already submitting');
    }
  }
  
  discard() {
    this.router.navigateByUrl('/dashboard');
  }
  navigateToUserProfile(): void {
    this.router.navigate(['/profile']);
  }
  navigateToDashboard(): void {
    this.router.navigate(['/dashboard']);
  }
  logout(): void {
    localStorage.removeItem('userRole');
    localStorage.removeItem('userId');
    localStorage.removeItem('searchval');
    localStorage.removeItem('userDetails');
    localStorage.clear;
    this.router.navigate(['/signup']);
  }
}
