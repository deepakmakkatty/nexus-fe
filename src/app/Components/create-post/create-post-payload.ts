export class CreatePostPayload {
    postName?: string;
    groupName?: string;
    description?: string;
    username?: string;
}