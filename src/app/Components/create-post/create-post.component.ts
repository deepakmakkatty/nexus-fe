import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { SubredditModel } from '../../Services/subreddit.response';
import { SubredditService } from '../../Services/subreddit.service';
import { CreatePostPayload } from './create-post-payload';
import { throwError } from 'rxjs';
import { PostService } from '../../Services/post.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  
  createPostForm!: FormGroup;
  postPayload: CreatePostPayload;
  subreddits!: Array<SubredditModel>;

  constructor(private router: Router, private postService: PostService,
              private subredditService: SubredditService) {
    this.postPayload = {
      postName: '',
      groupName: '',
      description: '',
      username: ''
    }
  }

  ngOnInit() {
    let userId = localStorage.getItem('userId');
    this.createPostForm = new FormGroup({
      postName: new FormControl('', Validators.required),
      groupName: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });
    this.subredditService.getAllSubreddits().subscribe((data) => {
      this.subreddits = data;
    }, error => {
      throwError(error);
    });
  }

  createPost() {
    let userId = localStorage.getItem('userId');
    if (userId !== null) {
      this.postPayload.username = userId;
    }
    this.postPayload.postName = this.createPostForm.get('postName')?.value;
    this.postPayload.groupName = this.createPostForm.get('groupName')?.value;
    this.postPayload.description = this.createPostForm.get('description')?.value;


    this.postService.createPost(this.postPayload).subscribe((data: any) => {
      this.router.navigateByUrl('/dashboard');
    }, (error: any) => {
      throwError(error);
      alert("Please log in to add a post")
    })
  }

  discardPost() {
    this.router.navigateByUrl('/dashboard');
  }
  navigateToUserProfile(): void {
    this.router.navigate(['/profile']);
  }
  navigateToDashboard(): void {
    this.router.navigate(['/dashboard']);
  }
  logout(): void {
    localStorage.removeItem('userRole');
    localStorage.removeItem('userId');
    localStorage.removeItem('searchval');
    localStorage.removeItem('userDetails');
    localStorage.clear;
    this.router.navigate(['/signup']);
  }
}
