import { Component } from '@angular/core';
import { UserProfileService } from '../../Components/user-profile/user-profile.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user-profile',
  templateUrl: './create-user-profile.component.html',
  styleUrls: ['./create-user-profile.component.css']
})
export class CreateUserProfileComponent {
  newUserProfile: any = {
    fullName: '',
    employeeId: '', 
    gender: '',
    dob: '',
    officeLocation: '',
    lineManager: '',
    employeeStatus: '',
    currentProject: '',
    jobTitle:'',
    startDate:'',
    bloodGroup:'',
    currentRole:'',
    programmingLanguages:''
  };

  constructor(
    private userProfileService: UserProfileService,
    private router : Router
    ) {}

  createUserProfile(): void {
    // Ensure employeeId is provided before making the request
    if (!this.newUserProfile.employeeId) {
      console.error('Employee ID is required.');
      return;
    }

    this.userProfileService.createUserProfile(this.newUserProfile).subscribe(
      () => {
        console.log('User profile created successfully!');
        console.log(this.newUserProfile);
        this.router.navigate(['/profile']);
      },
      (error) => {
        console.error('Error creating user profile:', error);
      }
    );
  }
  goboack(){
    this.router.navigate(['/profile']);
  }
}
