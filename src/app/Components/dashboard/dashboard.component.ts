import { Component, OnInit, ɵɵqueryRefresh } from '@angular/core';
import { Router } from '@angular/router';
let conditionBlockValue: string ;
import { SearchService } from '../../Services/search.service';
import { UserProfileService } from '../user-profile/user-profile.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{
  user:any;
  constructor(private router: Router,
    private userProfileService : UserProfileService) {}
  ngOnInit(): void {
    localStorage.removeItem('searchval');
    this.fetchUserProfileData();
  }
  conditionBlockValue = 'one'

  navigateToUserProfile(): void {
    this.router.navigate(['/profile']);
    conditionBlockValue = 'two'    
  }

  navigateToProjectSpaces(): void {
    this.router.navigate(['/project-spaces']);
  }

  navigateToCollaborativeSpaces(): void {
    this.router.navigate(['/collaborative-spaces']);
  }

  logout(): void {
    localStorage.removeItem('userRole');
    localStorage.removeItem('userId');
    localStorage.removeItem('searchval');
    localStorage.removeItem('userDetails');
    this.router.navigate(['/signup']);
  }
  fetchUserProfileData(): void {
    const userId = localStorage.getItem('userId',); 
    if(userId){
    this.userProfileService.getUserProfile(userId).subscribe(
      (profileData: any) => {
        this.user = profileData;
      },
      (error) => {
        console.error('Error fetching user profile:', error);
      }
    );
    }
  }
}
