import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../user-profile/user-profile.service';
import { SearchService } from '../../Services/search.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showProfileOptions: boolean = true;
  userProfile: any = {};
  searchResults: any[] = [];
  darkMode: boolean = true;
  searchQuery: string = '';

  constructor(
    private userProfileService: UserProfileService,
    private searchService: SearchService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.fetchUserProfileData();
  }

  fetchUserProfileData(): void {
    const userId = localStorage.getItem('userId'); 
    if(userId){
    this.userProfileService.getUserProfile(userId).subscribe(
      (profileData: any) => {
        this.userProfile = profileData;
      },
      (error) => {
        console.error('Error fetching user profile:', error);
      }
    );
    }
  }

  toggleProfileOptions(): void {
    this.showProfileOptions = !this.showProfileOptions;
  }

  viewProfile(): void {
    console.log('View Profile');
    this.router.navigate(['/profile']);
  }

  leavetrack(): void {
    console.log('leavetrack');
    alert("leavetrack is not configured.")
  }

  birthday(): void {
    console.log('birthday');
    alert("Birthday is not configured.")
  }

  logout(): void {
    localStorage.removeItem('userRole');
    localStorage.removeItem('userDetails');
    localStorage.removeItem('userId');
    this.router.navigate(['/signup']);
  }

  toggleDarkMode(): void {
    this.darkMode = !this.darkMode;
  }

  performSearch(): void {
    this.searchService.search(this.searchQuery).subscribe(
      (results: any[]) => {
        this.searchResults = results;
        console.log(this.searchResults[0]?.fullName);
        localStorage.setItem('searchResults',JSON.stringify(this.searchResults))
        // Navigate to the search results page only after the search results are fetched
        this.viewSearchResults();
      },
      (error) => {
        console.error('Error fetching search results:', error);
        this.searchResults = []; // Clear search results in case of error
      }
    );
  }

  showNotification(): void {
    alert("No Notification Yet Please configure Notifications Settings")
  }

  viewSearchResults(): void {
    const navigationExtras: NavigationExtras = {
      state: {
        searchResults: this.searchResults
      }
    };
    this.router.navigate(['/search-user-profile'], navigationExtras);
  }
  navigateToDashboard(){
    this.router.navigate(['/dashboard']);
  }
}
