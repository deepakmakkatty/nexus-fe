import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveShiftTrackingComponent } from './leave-shift-tracking.component';

describe('LeaveShiftTrackingComponent', () => {
  let component: LeaveShiftTrackingComponent;
  let fixture: ComponentFixture<LeaveShiftTrackingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaveShiftTrackingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LeaveShiftTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
