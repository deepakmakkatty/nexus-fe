import { Component, OnInit } from '@angular/core';
import { SubredditModel } from '../../Services/subreddit.response';
import { SubredditService } from '../../Services/subreddit.service';
import { throwError } from 'rxjs';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-list-group',
  templateUrl: './list-group.component.html',
  styleUrls: ['./list-group.component.css']
})
export class ListGroupComponent implements OnInit {

  subreddits!: Array<SubredditModel>;
  constructor(
    private subredditService: SubredditService,
    private router :Router
    ) { }

  ngOnInit() {
    this.subredditService.getAllSubreddits().subscribe(data => {
      this.subreddits = data;
    }, error => {
      throwError(error);
    })
  }
  
  navigateToUserProfile(): void {
    this.router.navigate(['/profile']);
  }
  navigateToDashboard(): void {
    this.router.navigate(['/dashboard']);
  }
  logout(): void {
    localStorage.removeItem('userRole');
    localStorage.removeItem('userId');
    localStorage.removeItem('searchval');
    localStorage.removeItem('userDetails');
    localStorage.clear;
    this.router.navigate(['/signup']);
  }
}