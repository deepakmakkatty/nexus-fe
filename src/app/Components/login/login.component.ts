import { Component } from '@angular/core';
import { AuthenticationService } from '../../Services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username: string = '';
  password: string = '';

  constructor(private authService: AuthenticationService, private router: Router) {}

  login(): void {
    this.authService.login({ username: this.username, password: this.password })
      .subscribe(
        (response) => {
          localStorage.setItem('userDetails', JSON.stringify(response));
          this.router.navigate(['/dashboard']);
        },
        (error) => {
          alert('Invalid credentials. Please try again.');
        }
      );
  }
}
