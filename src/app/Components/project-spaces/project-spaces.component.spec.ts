import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectSpacesComponent } from './project-spaces.component';

describe('ProjectSpacesComponent', () => {
  let component: ProjectSpacesComponent;
  let fixture: ComponentFixture<ProjectSpacesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectSpacesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectSpacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
