import { Component, OnInit } from '@angular/core';
import { ProjectSpacesService } from './project-spaces.service';

@Component({
  selector: 'app-project-spaces',
  templateUrl: './project-spaces.component.html',
  styleUrls: ['./project-spaces.component.css']
})
export class ProjectSpacesComponent implements OnInit {
  projectGroups: any[] = [];

  constructor(private projectSpacesService: ProjectSpacesService) {}

  ngOnInit(): void {
    this.fetchProjectGroups();
  }
  project : any [] = ["NUUDAY", "CSRD", "O2UK" , "T-Mobile" , "Etisalat" ,"Telefonica", "Telekom Malaysia","Bharti Airtel","Cisco"] 

  fetchProjectGroups(): void {
    this.projectSpacesService.getProjectGroups().subscribe(groups => {
      this.projectGroups = groups;
    });
  }
}
