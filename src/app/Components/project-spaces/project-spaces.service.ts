import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectSpacesService {
  private apiUrl = 'your_backend_api/project-spaces';

  constructor(private http: HttpClient) {}

  getProjectGroups(): Observable<any[]> {
    // Implement logic to fetch project groups from the backend
    return this.http.get<any[]>(`${this.apiUrl}/get-groups`);
  }

  createProjectGroup(groupData: any): Observable<any> {
    // Implement logic to create a new project group and communicate with the backend
    return this.http.post(`${this.apiUrl}/create-group`, groupData);
  }
}
