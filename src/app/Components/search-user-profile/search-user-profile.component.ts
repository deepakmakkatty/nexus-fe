import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-search-user-profile',
  templateUrl: './search-user-profile.component.html',
  styleUrls: ['./search-user-profile.component.css']
})
export class SearchUserProfileComponent implements OnInit {
  searchResults: any[] = [];

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    localStorage.removeItem('searchval');
    this.extractSearchResults();
    const searchResultsString = localStorage.getItem('searchResults');
    if (searchResultsString !== null) {
      this.searchResults = JSON.parse(searchResultsString);
      console.log(this.searchResults)
    }
  }
  

  extractSearchResults(): void {
    this.route.queryParams.subscribe(params => {
      // Guard against undefined or null
      if (params && params['state'] && params['state'].searchResults) {
        this.searchResults = params['state'].searchResults;
        console.log(this.searchResults)
      }
    });
  }

  navigateToUserProfile(userId: string): void {
    // Navigate to the user profile page with the userId
    // this.router.navigate(['/user-profile', userId]);
    this.router.navigate(['/profile']);
    localStorage.setItem('searchval',userId)
  }
}
