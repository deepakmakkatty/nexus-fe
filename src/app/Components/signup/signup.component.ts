import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../Services/authentication.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  username: string = '';
  password: string = '';
  emailId: string = '';
  containerClass: string = ''; 

  constructor(private authService: AuthenticationService, private router: Router) {}
  
  ngOnInit(): void {
    localStorage.clear();
}

  signup(): void {
    const user = {
      username: this.username,
      password: this.password,
      emailId: this.emailId
    };

    this.authService.register(user).subscribe(
      (response: HttpResponse<any>) => {
        if (response.body.message === 'user saved successfully') {
          alert('User registered successfully!');
          this.router.navigate(['/createuser']);
        } else {
          console.error('Unexpected response:', response);
          alert('Error registering user. Please try again. Check console for details.');
        }
      },
      (error) => {
        console.error('Error registering user:', error);
        alert('Error registering user. Please try again. Check console for details.');
      }
    );
  }

  signin(): void {
    this.authService.login({ username: this.username, password: this.password })
      .subscribe(
        (response) => {
          localStorage.setItem('userDetails', JSON.stringify(response));
          this.router.navigate(['/dashboard']);
          localStorage.setItem('userId', this.username);
          if (this.username === "userrolehr") {
            localStorage.removeItem('userRole');
            localStorage.setItem('userRole', 'HR');
          }
          else{
            localStorage.setItem('userRole', 'user');
          }
        },
        (error) => {
          alert('Invalid credentials. Please try again.');
        }
      );
  }

  togglePanel(panel: 'sign-in' | 'sign-up'): void {
    // Toggle the container class based on the panel argument
    if (panel === 'sign-in') {
      this.containerClass = '';
    } else {
      this.containerClass = 'right-panel-active';
    }
  }
}
