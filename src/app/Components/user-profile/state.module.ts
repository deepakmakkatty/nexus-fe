import { NgModule } from '@angular/core';
import { StateService } from './state.service'; // Adjust the path based on your actual structure

@NgModule({
  providers: [StateService]
})
export class StateModule { }
