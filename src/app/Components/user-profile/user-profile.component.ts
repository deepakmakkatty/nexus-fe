import { Component, OnInit } from '@angular/core';
import { UserProfileService } from './user-profile.service';
import { Router } from '@angular/router';
import { ProjectInfoService } from '../../Services/project-info.service';
import { GenerateResumeService } from '../../Services/generate-resume.service'; 

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  userProfile: any;
  editing: boolean = false;
  projectInfoDisplay = false;
  userInfoDisplay = true;
  projectInformation: any;

  constructor(
    private userProfileService: UserProfileService,
    private router: Router,
    private projectInfoService: ProjectInfoService,
    private generateResumeService: GenerateResumeService // Inject GenerateResumeService
  ) {}

  ngOnInit(): void {
    // Fetch user profile data on component initialization
    this.fetchUserProfile();
    this.fetchProjectInfo();
  }

  fetchUserProfile(): void {
    let userId = localStorage.getItem('userId');
    const searchval = localStorage.getItem('searchval');
    userId = searchval ?? userId;

    if (userId) {
      this.userProfileService.getUserProfile(userId).subscribe(
        (profileData: any) => {
          this.userProfile = profileData;
        },
        (error) => {
          console.error('Error fetching user profile:', error);
        }
      );
    }
  }

  fetchProjectInfo(): void {
    let userId = localStorage.getItem('userId');
    const searchval = localStorage.getItem('searchval');
    userId = searchval ?? userId;

    if (userId) {
      this.projectInfoService.fetchProjectInfo(userId).subscribe(
        (data: any) => {
          this.projectInformation = data;
          console.log(this.projectInformation);
        },
        (error) => {
          console.error('Error fetching project information:', error);
        }
      );
    }
  }

  toggleEditing(): void {
    this.editing = !this.editing;
  }

  saveUserProfile(): void {
    const userId = JSON.stringify(localStorage.getItem('userId'));
    this.userProfileService.updateUserProfile(userId, this.userProfile).subscribe(
      () => {
        console.log('User profile updated successfully!');
        this.editing = false; 
      },
      (error) => {
        console.error('Error updating user profile:', error);
      }
    );
  }

  createUser(): void {
    this.router.navigate(['/createuser']);
  }

  goBack(): void {
    this.router.navigate(['/dashboard']);
  }

  projectInfoDisplayfun(): void {
    this.projectInfoDisplay = true;
    this.userInfoDisplay = false;
  }

  userInfoDisplayfun(): void {
    this.projectInfoDisplay = false;
    this.userInfoDisplay = true;
  }

  generateResume(): void {
    let userId = localStorage.getItem('userId');
    const searchval = localStorage.getItem('searchval');
    userId = searchval ?? userId;

    if (userId) {
      this.generateResumeService.generateResume(userId).subscribe(
        (resumeData: any) => {
          // Handle response from the service if needed
          const blob = new Blob([resumeData], { type: 'application/docx' }); // Adjust the type accordingly
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(blob);
          downloadLink.download = `resume_${userId}.docx`; // Adjust the filename
          document.body.appendChild(downloadLink);
          downloadLink.click();
          document.body.removeChild(downloadLink);
          console.log('Resume generated successfully:', resumeData);
        },
        (error) => {
          console.error('Error generating resume:', error);
        }
      );
    }
  }

  downloadResume(): void {
    let userId = localStorage.getItem('userId');
    const searchval = localStorage.getItem('searchval');
    userId = searchval ?? userId;

    if (userId) {
      const downloadUrl = `http://localhost:8096/api/getResume/${userId}`;
      window.open(downloadUrl, '_blank');
    }
  }
}
