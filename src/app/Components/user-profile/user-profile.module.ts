import { NgModule } from '@angular/core';
import { UserProfileComponent } from './user-profile.component';
import { StateModule } from './state.module';

@NgModule({
  declarations: [UserProfileComponent],
  imports: [StateModule],
  exports: [UserProfileComponent]
})
export class UserProfileModule { }
