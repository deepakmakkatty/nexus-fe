import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  private apiUrl = 'http://localhost:8096/api';

  constructor(private http: HttpClient) {}

  getUserProfile(userId: string): Observable<any> {
    return this.http.get(`${this.apiUrl}/userProfile/${userId}`);
  }

  updateUserProfile(userId: string, updatedProfile: any): Observable<any> {
    return this.http.patch(`${this.apiUrl}/userProfile/${userId}`, updatedProfile);
  }

  generateResume(): Observable<any> {
    return this.http.get(`${this.apiUrl}/generateResume`);
  }

  createUserProfile(newProfile: any): Observable<any> {
    return this.http.post(`${this.apiUrl}/CreateProfile`, newProfile);
  }
}
