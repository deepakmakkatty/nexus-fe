export interface GroupModel {
    id: number;
    groupName: string;
    employeeId: number;
    userName: string;
  }
  