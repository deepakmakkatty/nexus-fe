import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupService } from '../../Services/group.service';
import { GroupModel } from './group-model';
import { Router } from '@angular/router';
import { PostModel } from '../../shared/post-model'; 
import { PostService } from '../../shared/post.service'; 
import { faComments } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-view-group',
  templateUrl: './view-group.component.html',
  styleUrls: ['./view-group.component.css']
})
export class ViewGroupComponent implements OnInit {
  groupId: number | undefined;
  groupDetails: GroupModel | undefined;
  posts$: Array<PostModel> = [];
  groupPosts: PostModel[] = [];
  visiblePosts: PostModel[] = [];
  faComments = faComments;
  currentPage: number = 1;
  pageSize: number = 2;

  constructor(
    private route: ActivatedRoute, 
    private groupService: GroupService,
    private postService: PostService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.groupId = +params['id'];
      if (this.groupId) {
        this.loadGroupDetails();
        this.loadPosts();
      }
    });
  }

  loadGroupDetails(): void {
    if (this.groupId) {
      this.groupService.getGroupById(this.groupId).subscribe(
        (data: GroupModel) => {
          this.groupDetails = data;
        },
        (error) => {
          console.error('Error fetching group details:', error);
        }
      );
    }
  }

  loadPosts(): void {
    this.postService.getAllPosts().subscribe((posts: Array<PostModel>) => {
      this.posts$ = posts;
      this.updateGroupPosts();
    });
  }

  updateGroupPosts(): void {
    if (this.groupId) {
      this.groupPosts = this.posts$.filter(post => post.groupId === this.groupId);
      this.updateVisiblePosts();
    }
  }

  updateVisiblePosts(): void {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    this.visiblePosts = this.groupPosts.slice(startIndex, startIndex + this.pageSize);
  }

  goToPost(id: number): void {
    this.router.navigateByUrl('/view-post/' + id);
  }

  navigateToUserProfile(): void {
    this.router.navigate(['/profile']);
  }

  navigateToDashboard(): void {
    this.router.navigate(['/dashboard']);
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/signup']);
  }

  previousPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.updateVisiblePosts();
    }
  }

  nextPage(): void {
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.updateVisiblePosts();
    }
  }

  get totalPages(): number {
    return Math.ceil(this.groupPosts.length / this.pageSize);
  }
}
