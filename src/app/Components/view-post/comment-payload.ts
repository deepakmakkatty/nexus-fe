// comment-payload.ts

export interface CommentPayload {
    text: string;
    postId: string;
    userId?: string;
    username?: string;
    duration?: string; // Add the duration property
  }
  