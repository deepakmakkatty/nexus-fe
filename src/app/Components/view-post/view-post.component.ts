import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/shared/post.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PostModel } from 'src/app/shared/post-model';
import { catchError } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommentPayload } from '../../Components/view-post/comment-payload';
import { CommentService } from '../../Services/comment.service';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css']
})
export class ViewPostComponent implements OnInit {

  postId: number;
  post?: PostModel;
  commentForm: FormGroup;
  commentPayload: CommentPayload;
  comments: CommentPayload[] = [];

  constructor(private postService: PostService, private activateRoute: ActivatedRoute,
    private commentService: CommentService, private router: Router) {
    this.postId = +this.activateRoute.snapshot.params['id']; // Convert to number using '+'

    this.commentForm = new FormGroup({
      text: new FormControl('', Validators.required)
    });
    this.commentPayload = {
      text: '',
      postId: this.postId.toString(),
      
    };
  }

  ngOnInit(): void {
    this.getPostById();
    this.getCommentsForPost();
    let userId = localStorage.getItem('userId');

  }
  
  postComment() {
    // Fetch userId from local storage
    let userId = localStorage.getItem('userId');
  
    // Check if userId is null before setting it in the comment payload
    if (userId !== null) {
      // Set the userId in the comment payload
      this.commentPayload.text = this.commentForm.get('text')?.value;
      this.commentPayload.userId = userId; // Add userId to the payload
  
      this.commentService.postComment(this.commentPayload).subscribe(() => {
        this.commentForm.get('text')?.setValue('');
        this.getCommentsForPost();
      }, (error: any) => {
        console.error('Error posting comment:', error);
      });
    } else {
      console.error('User ID not found in local storage');
      // Handle the case where userId is null (e.g., display an error message)
    }
  }
  
  

  private getPostById() {
    this.postService.getPost(this.postId).subscribe((data: PostModel) => {
      this.post = data;
    }, (error: any) => { // Define 'error' parameter type explicitly
      console.error('Error fetching post:', error);
    });
  }

  private getCommentsForPost() {
    this.commentService.getAllCommentsForPost(this.postId).subscribe((data: CommentPayload[]) => {
      this.comments = data;
    }, (error: any) => { // Define 'error' parameter type explicitly
      console.error('Error fetching comments:', error);
    });
  }
  navigateToUserProfile(): void {
    this.router.navigate(['/profile']);
  }
  navigateToDashboard(): void {
    this.router.navigate(['/dashboard']);
  }
  logout(): void {
    localStorage.removeItem('userRole');
    localStorage.removeItem('userId');
    localStorage.removeItem('searchval');
    localStorage.removeItem('userDetails');
    localStorage.clear;
    this.router.navigate(['/signup']);
  }
}
