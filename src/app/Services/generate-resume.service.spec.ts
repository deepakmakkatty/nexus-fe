import { TestBed } from '@angular/core/testing';

import { GenerateResumeService } from './generate-resume.service';

describe('GenerateResumeService', () => {
  let service: GenerateResumeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenerateResumeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
