import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GenerateResumeService {

  constructor(private http: HttpClient) { }

  generateResume(userId: string) {
    // Adjust the URL to include the userId
    const url = `http://localhost:8096/api/getResume/${userId}`;

    // Make a GET request
    return this.http.get(url, { responseType: 'blob' });
  } 
}
