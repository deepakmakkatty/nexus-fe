import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GroupModel } from '../Components/view-group/group-model';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  private baseUrl = 'http://localhost:8096/api';

  constructor(private http: HttpClient) { }

  getGroupById(groupId: number): Observable<GroupModel> {
    return this.http.get<GroupModel>(`${this.baseUrl}/getByGroupId/${groupId}`);
  }
}
