import { TestBed } from '@angular/core/testing';

import { LeaveShiftTrackingService } from './leave-shift-tracking.service';

describe('LeaveShiftTrackingService', () => {
  let service: LeaveShiftTrackingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LeaveShiftTrackingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
