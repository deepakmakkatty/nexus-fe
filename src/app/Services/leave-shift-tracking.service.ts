import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LeaveShiftTrackingService {
  private apiUrl = 'your_backend_api/leave-shift';

  constructor(private http: HttpClient) {}

  trackLeave(): Observable<any> {
    // Implement logic to track leave and communicate with the backend
    return this.http.post(`${this.apiUrl}/track-leave`, {});
  }

  trackShift(): Observable<any> {
    // Implement logic to track shift and communicate with the backend
    return this.http.post(`${this.apiUrl}/track-shift`, {});
  }

  exportToExcel(): Observable<any> {
    // Implement logic to export data to Excel and communicate with the backend
    return this.http.get(`${this.apiUrl}/export-to-excel`);
  }
}
