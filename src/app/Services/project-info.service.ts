import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProjectInfoService {

  constructor(private http: HttpClient) { }

  fetchProjectInfo(userId: string) {
    // Pass userId as a parameter in the URL or as part of the request body if needed
    return this.http.get<any>('http://localhost:8096/api/getProjectInformationByEmp/' + userId);
  }
}
