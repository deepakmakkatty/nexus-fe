// search.service.ts

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private searchUrl = 'http://localhost:8096/api/searchUser'; // Update the API endpoint

  constructor(private http: HttpClient) {}

  search(query: string): Observable<any[]> {
    const url = `${this.searchUrl}/${query}`; // Append the query to the endpoint
    return this.http.get<any[]>(url);
  }
}
