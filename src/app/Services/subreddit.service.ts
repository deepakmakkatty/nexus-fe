import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SubredditModel } from './subreddit.response';

@Injectable({
  providedIn: 'root'
})
export class SubredditService {
  constructor(private http: HttpClient) { }

  getAllSubreddits(): Observable<Array<SubredditModel>> {
    return this.http.get<Array<SubredditModel>>('http://localhost:8096/api/getAllGroups');
  }

  createSubreddit(subreddit: SubredditModel): Observable<any> {
    return this.http.post<any>('http://localhost:8096/api/createGroup', subreddit);
  }
}
