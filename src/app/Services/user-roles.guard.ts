import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

interface MyRoute extends ActivatedRouteSnapshot {
  data: {
    roles: string[];
  };
}

@Injectable({
  providedIn: 'root'
})
export class UserRolesGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(route: MyRoute, state: RouterStateSnapshot): boolean {
    const userRole = localStorage.getItem('userRole');

    if (userRole) {
      if (route.data && route.data.roles && route.data.roles.indexOf(userRole) === -1) {
        alert('You do not have permission to access this page.');
        // this.router.navigate(['/signup']);
        return false;
      }
      return true;
    } else {
      this.router.navigate(['/signup']);
      return false;
    }
  }
}
