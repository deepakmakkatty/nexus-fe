import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserProfileComponent } from './Components/user-profile/user-profile.component';
import { ProjectSpacesComponent } from './Components/project-spaces/project-spaces.component';
import { CollaborativeSpacesComponent } from './Components/collaborative-spaces/collaborative-spaces.component';
import { LoginComponent } from './Components/login/login.component';
import { SignupComponent } from './Components/signup/signup.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { UserRolesGuard } from './Services/user-roles.guard';
import { CreateUserProfileComponent } from './Components/create-user-profile/create-user-profile.component';
import { HomeComponent } from './Components/home/home.component';
import { CreateGroupComponent } from './Components/create-group/create-group.component';
import { CreatePostComponent } from './Components/create-post/create-post.component';
import { ListGroupComponent } from './Components/list-group/list-group.component';
import { SearchUserProfileComponent } from './Components/search-user-profile/search-user-profile.component';
import { ViewPostComponent } from '././Components/view-post/view-post.component';
import { ViewGroupComponent } from './Components/view-group/view-group.component';

const routes: Routes = [
  { path: 'profile', component: UserProfileComponent },
  { path: 'project-spaces', component: ProjectSpacesComponent },
  { path: 'collaborative-spaces', component: CollaborativeSpacesComponent },
  { path: 'search-user-profile', component: SearchUserProfileComponent },
  { path: 'view-post/:id', component: ViewPostComponent },

  // Add more routes as needed

  // New routes for login, signup, and dashboard
  { path: 'login', component: LoginComponent },
  { 
    path: 'signup', component: SignupComponent },
  { 
    path: 'createuser', 
    component: CreateUserProfileComponent,
    canActivate: [UserRolesGuard],
    data: { roles: ['HR'] }
  },
  { path: 'home', component: HomeComponent },//remove this later
  { path: 'createGroup', component: CreateGroupComponent },
  { path: 'createPost', component: CreatePostComponent },
  { path: 'list-group', component: ListGroupComponent },
  { path: 'app-create-group', component: CreateGroupComponent },
  { path: 'user-profile/:name', component: UserProfileComponent},
  { path: 'view-group/:id', component: ViewGroupComponent },

  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [UserRolesGuard],
    data: { roles: ['user', 'admin','HR'] } // Define roles that can access this route
  },

  // Redirect to the login page if no route matches
  { path: '', redirectTo: '/signup', pathMatch: 'full' },

  // Catch-all route for unknown paths, redirect to login
  { path: '**', redirectTo: '/signup' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
