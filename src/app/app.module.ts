import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserProfileComponent } from './Components/user-profile/user-profile.component';
import { ProjectSpacesComponent } from './Components/project-spaces/project-spaces.component';
import { CollaborativeSpacesComponent } from './Components/collaborative-spaces/collaborative-spaces.component';
import { LeaveShiftTrackingComponent } from './Components/leave-shift-tracking/leave-shift-tracking.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './Components/login/login.component';
import { SignupComponent } from './Components/signup/signup.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './Components/header/header.component';
import { CreateUserProfileComponent } from './Components/create-user-profile/create-user-profile.component';
import { HomeComponent } from './Components/home/home.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PostTileComponent } from './shared/post-tile/post-tile.component';
import { VoteButtonComponent } from './shared/vote-button/vote-button.component';
import { SideBarComponent } from './shared/side-bar/side-bar.component';
import { SubredditSideBarComponent } from './shared/subreddit-side-bar/subreddit-side-bar.component';
import { CreateGroupComponent } from './Components/create-group/create-group.component';
import { CreatePostComponent } from './Components/create-post/create-post.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ListGroupComponent } from './Components/list-group/list-group.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { SearchUserProfileComponent } from './Components/search-user-profile/search-user-profile.component';
import { ViewPostComponent } from './Components/view-post/view-post.component';
import { ViewGroupComponent } from './Components/view-group/view-group.component';

@NgModule({
  declarations: [
    AppComponent,
    UserProfileComponent,
    ProjectSpacesComponent,
    CollaborativeSpacesComponent,
    LeaveShiftTrackingComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    HeaderComponent,
    CreateUserProfileComponent,
    HomeComponent,
    PostTileComponent,
    VoteButtonComponent,
    SideBarComponent,
    SubredditSideBarComponent,
    CreateGroupComponent,
    CreatePostComponent,
    ListGroupComponent,
    SearchUserProfileComponent,
    ViewPostComponent,
    ViewGroupComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    EditorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
