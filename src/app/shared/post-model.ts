export class PostModel {
    id: any;
    postName: any;
    url: any;
    description: any;
    voteCount: any;
    userName: any;
    groupName: any;
    commentCount: any;
    duration: any;
    employeeId: any;
    groupId: any;
}