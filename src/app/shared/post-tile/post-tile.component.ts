import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PostService } from '../post.service';
import { PostModel } from '../post-model';
import { faComments } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { UserProfileService } from '../../Components/user-profile/user-profile.service';
import { SubredditService } from '../../Services/subreddit.service'; 


interface Employee {
  employeeId: number;
  userId: string;
  name: string;
}

const employees: Employee[] = [
  { employeeId: 1, userId: 'dilp0922', name: 'Dileep' },
  { employeeId: 2, userId: 'demt0922', name: 'Deepak M T' },
  { employeeId: 52, userId: 'viku0622', name: 'Vijay Kumar' },
  { employeeId: 952, userId: 'john0922', name: 'John' },
  { employeeId: 953, userId: 'john0921', name: 'John' },
  { employeeId: 954, userId: 'john0111', name: 'John' },
  { employeeId: 955, userId: 'userrolehrtest2', name: 'John' },
  { employeeId: 1952, userId: 'prch1217', name: 'Pradyumna' }
];

@Component({
  selector: 'app-post-tile',
  templateUrl: './post-tile.component.html',
  styleUrls: ['./post-tile.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class PostTileComponent implements OnInit {
  posts$: Array<PostModel> = [];
  visiblePosts: PostModel[] = [];
  faComments = faComments;
  currentPage: number = 0;
  groupNames: { [key: number]: string } = {};

  constructor(
    private postService: PostService,
    private router: Router,
    private userProfileService: UserProfileService, // Inject UserProfileService
    private subredditService: SubredditService // Inject SubredditService
  ) { }

  ngOnInit(): void {
    this.postService.getAllPosts().subscribe(posts => {
      this.posts$ = posts;
      this.updateVisiblePosts();
    });

    // Fetch group names
    this.subredditService.getAllSubreddits().subscribe(groups => {
      groups.forEach(group => {
        this.groupNames[group.id] = group.groupName;
      });
    });
  }

  goToPost(id: number): void {
    this.router.navigateByUrl('/view-post/' + id);
  }

  nextPage(): void {
    this.currentPage++;
    this.updateVisiblePosts();
  }

  previousPage(): void {
    this.currentPage--;
    this.updateVisiblePosts();
  }

  updateVisiblePosts(): void {
    const startIndex = this.currentPage * 3;
    this.visiblePosts = this.posts$.slice(startIndex, startIndex + 3);
  }

  getEmployeeName(employeeId: number): string {
    const employee = employees.find(emp => emp.employeeId == employeeId);
    return employee ? employee.name : '';
  }

  getGroupName(groupId: number): string {
    return this.groupNames[groupId] || '';
  }
  postNavigate(groupIdlocal: any): void {
    console.log(groupIdlocal);
    this.router.navigateByUrl(`/view-group/${groupIdlocal}`);
  }
  navigateUserProfile(employeeId: string): void {
    const user = employees.find(emp => emp.employeeId.toString() === employeeId);
    if (user) {
      localStorage.setItem('searchval', user.userId);
      this.router.navigateByUrl(`/profile`);
    } else {
      console.error('User not found');
    }
  }
  
}
