import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PostModel } from './post-model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getAllPosts(): Observable<Array<PostModel>> {
    return this.http.get<Array<PostModel>>('http://localhost:8096/api/posts/getAllPosts');
  }

  getPost(postId: number): Observable<PostModel> {
    return this.http.get<PostModel>(`http://localhost:8096/api/posts/getPostById/${postId}`);
  }
}
