import { Component, Input, OnInit } from '@angular/core';
import { PostModel } from '../post-model';
import { faArrowUp , faArrowDown, faComment } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-vote-button',
  templateUrl: './vote-button.component.html',
  styleUrls: ['./vote-button.component.css']
})
export class VoteButtonComponent implements OnInit {

  @Input() post!: PostModel; // Add '!' to inform TypeScript that the property will be initialized later

  faArrowUp = faArrowUp;
  faArrowDown = faArrowDown;
  faComment = faComment;

  constructor() { }

  ngOnInit(): void {
  }

  upvotePost() {
    // Implement upvote logic here
  }

  downvotePost() {
    // Implement downvote logic here
  }

  goToPost(post: any) {
    // Implement navigation logic here
  }
}
